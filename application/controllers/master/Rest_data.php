<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Rest_data extends REST_Controller{
    function __construct(){
        parent::__construct();
    }

    public function buku_get($id){
        $this->response([
            'status' => true,
            'message' => $id
        ], REST_Controller::HTTP_OK);
    }

    public function buku_post(){
        $str = $this->post('arr_data');
        echo var_dump($str);
        // $str = json_decode($this->post('data'));
        // $this->response([
        //     'status' => true,
        //     'message' => $str
        // ], REST_Controller::HTTP_OK);
    }
}
?>